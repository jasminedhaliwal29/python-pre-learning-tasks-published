def calculator(a, b, operator):
    # ==============
    # Your code here
    if operator == "+":
        sum = a+b
    
    if operator == "*":
        sum = a*b
    
    if operator == "-":
        sum = a-b
    
    if operator == "/":
        sum = a/b
    
    y = ""

    while sum>0:
        if sum%2==0:
            y+="0"
            sum = sum/2
        else:
            y+="1"
            sum = (sum-1)/2
    
    return y[::-1]

    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
