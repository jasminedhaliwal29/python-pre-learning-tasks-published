def factors(number):
    
    # ==============
	factorsList = []

	for i in range(1,number+1):
		if number%i==0:
			factorsList.append(i)
			
	factorsList.remove(1)
	factorsList.remove(number)
	
	if len(factorsList) == 0:
	    return str(number) + ' is a prime number'
	else:
	    return factorsList
	
    # ==============


print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
