def factors(number):
    
    # ==============
	factorsList = []

	for i in range(1,number+1):
		if number%i==0:
			factorsList.append(i)
			
	factorsList.remove(1)
	factorsList.remove(number)

	return factorsList
	
    # ==============


print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
